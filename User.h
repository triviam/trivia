#pragma once

#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

#include "Room.h"
#include "Helper.h"
#include "Protocol.h"

using namespace std;

class User
{
	public:
		User(string, SOCKET);
		void send(string);
		string getUsername();
		SOCKET getSocket();
		Room* getRoom();
		//Game* getGame();
		//void setGame(Game*);
		void clearRoom();
		bool createRoom(int, string, int, int, int);
		bool joinRoom(Room*);
		void leaveRoom();
		int closeRoom();
		bool leaveGame();
	private:
		string _username;
		Room* _currRoom;
		//Game* _currGame;
		SOCKET _sock;

	
};




















