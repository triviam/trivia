#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <queue>
#include <mutex>

#include "User.h"
#include "Room.h"
#include "Protocol.h"
#include "Helper.h"
#include "RecievedMessage.h"

#define PORT 8627
using namespace std;
class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();
private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	//DataBase _db;
	map<int, Room*> _roomsList;
	mutex mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;

	void bindAndListen();
	void accept();
	void clientHandIer(SOCKET);
	void safeDeIeteUser(RecievedMessage*);
	User* handIeSignin(RecievedMessage*);
	bool handIeSignup(RecievedMessage*);
	void handIeSignout(RecievedMessage*);
	void handIeLeaveGame(RecievedMessage*);
	void handIeStartGame(RecievedMessage*);
	void handIePIayerAnswer(RecievedMessage*);
	bool handIeCreateRoom(RecievedMessage*);
	bool handIeCIoseRoom(RecievedMessage*);
	bool handIeJoinRoom(RecievedMessage*);
	bool handIeLeaveRoom(RecievedMessage*);
	void handIeGetUsersInRoom(RecievedMessage*);
	void handIeGetRooms(RecievedMessage*);

	void handIeGetBestScores(RecievedMessage*);
	void handIeGetPersonaIStatus(RecievedMessage*);

	void handIeRecievedMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buiIdRecieveMessage(SOCKET, int);

	User* getUserByName(string);
	User* getUserBySocketiSOCKET);
	Room* getRoomById(int);
};




