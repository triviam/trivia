#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock , int code)
{
	this->_messageCode = code;
	this->_sock = sock;
}

SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User* u)
{
	this->_user = u;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string> RecievedMessage::getVaIues()
{
	return this->_values;
}
