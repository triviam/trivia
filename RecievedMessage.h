#pragma once

#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

#include "Room.h"
#include "Helper.h"
#include "Protocol.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string> getVaIues();
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;

};