#include "TriviaServer.h"

TriviaServer::TriviaServer()
{
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

TriviaServer::~TriviaServer()
{
	try
	{
		this->_connectedUsers.clear();
		this->_roomsList.clear();
		::closesocket(this->_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	bindAndListen();
	thread t(&TriviaServer::clientHandIer, this->_socket);
	while (true)
	{
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET; 
	sa.sin_addr.s_addr = INADDR_ANY;   
	if (::bind(this->_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	
	if (::listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << "Listening on port " << PORT << endl;

}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(this->_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw exception(__FUNCTION__);
	}
	cout << "Client accepted. Server and client can speak" << endl;
	thread t(&Server::clientHandler, this, client_socket);
	t.detach();
}
