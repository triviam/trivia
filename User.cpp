#include "User.h"

User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
	this->_currRoom = nullptr;
}

void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room * User::getRoom()
{
	return this->_currRoom;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom == nullptr)
	{
		this->_currRoom = new Room(roomId,this, roomName, maxUsers, questionTime, questionsNo);
		Helper::sendData(this->_sock, NEW_ROOM_COMM_SUCCESS);
		return true;
	}
	Helper::sendData(this->_sock, NEW_ROOM_COMM_FAIL);
	return false;
}

bool User::joinRoom(Room * r)
{
	if (this->_currRoom == nullptr)
	{
		bool result = this->_currRoom->joinRoom(this);
		return result;
	}
	return false;
}

void User::leaveRoom()
{
	if (this->_currRoom == nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (this->_currRoom != nullptr)
	{
		vector<User*>::iterator it;
		bool inRoom = false;
		for (it = this->_currRoom->getUsers().begin(); it != this->_currRoom->getUsers().end(); it++)
		{
			if (*it == this)
			{
				inRoom = true;
			}
		}
		if (inRoom)
		{
			return this->_currRoom->closeRoom(this);
		}
		else
		{
			return -1;
		}
	}
	return -1;
}


