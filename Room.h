#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "User.h"

using namespace std;

class Room
{
	private: 
		vector<User*> _users;
		User* _admin;
		int _maxUsers;
		int _questionTime;
		int _questionNo;
		string _name;
		int _id;

	public:
		Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionNo);
		string getUsersAsString(vector<User*> users, User * user);
		string getUsersListMessage();
		void SendMessage(User * excludeUser, string message);
		void SendMessage(string message);
		bool joinRoom(User * user);
		void leaveRoom(User * user);
		int closeRoom(User * user);

		vector<User*> getUsers();
		int getQuestionNo();
		int getId();
		string getName();




};